# IFT 712 - Leaf Classification Project 

## About this project
#### Data
This program was develop and tested for the "Leaf classification" challenge from Kaggle.  
Source : https://www.kaggle.com/c/leaf-classification/data

#### Features
Run several classification methods from the scikit-learn library on a dataset.  
* Compute precision and loss for each classifier.  
* Compute score matrix for Kaggle submission system.


## Getting started
### Prerequisites
This program requires the following environment to run
* Python 3
* Numpy
* Pandas 
* Scikit-learn

### Using the program
````
python main.py [-h] train_file test_file [--skip-comparison]
````

  
##### Positional arguments:  
| Argument | Description |
| :------- | :---------- |
| train_file  |  Training csv file path   |
| test_file   |  Testing csv file path  |
##### Optional arguments:  

| Argument | Description |
| :------- | :---------- |
| --skip-comparison  |  Only compute score matrix for kaggle submissions (Skip precision and log loss computation )   |
| -h, --help   |  show this help message and exit   |

### Example
If the training and testing files are stored under ./data/, the following command will run the classifiers on given files and compares the classifiers.

````
python main.py ./data/train.csv ./data/test.csv
````

To run the classifiers on the same files without comparison (Only compute score matrix for kaggle submissions (Skip precision and log loss computation for all classifiers for internal tests):

````
python main.py ./data/train.csv ./data/test.csv --skip_compare
````

## Authors

- Marie Degen - [Marie.Degen@Usherbrooke.ca](Marie.Degen@usherbrooke.ca)
- Paul-Antoine Dumergue - [Paul-Antoine.Dumergue@USherbrooke.ca](Paul-Antoine.Dumergue@USherbrooke.ca)




