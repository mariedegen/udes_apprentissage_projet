# -*- coding: utf-8 -*-
from sklearn.naive_bayes import GaussianNB

from classifiers.generic_classifier import GenericClassifier

"""
    Gaussian Naive Bayes Classifier
"""


class GaussianNaiveBayesClassifier(GenericClassifier):
    def __init__(self):
        """
        Constructor
        self.classifier : type of classifier
        :return:
        """
        super().__init__()

    def training(self, x_train, t_train):
        """
        Gaussian Naive Bayes classifier with scikit learn
        :param x_train: training set
        :param t_train: prediction
        """
        # init the Gaussian NB classifier and train
        # No GridSearch needed, there is no hyper parameter
        gaussian_nb_classifier = GaussianNB()
        gaussian_nb_classifier.fit(x_train, t_train)

        self.classifier = gaussian_nb_classifier
