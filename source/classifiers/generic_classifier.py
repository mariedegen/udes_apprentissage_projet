import abc

from sklearn.metrics import log_loss


class GenericClassifier(metaclass=abc.ABCMeta):
    def __init__(self):
        self.classifier = None

    @abc.abstractmethod
    def training(self, x_train, t_train):
        pass

    def prediction(self, x_test):
        """
        Generic Prediction function
        :param: x_test data to classify with a certain probability
        :return: the probability to belong to each class
        """
        return self.classifier.predict_proba(x_test)

    def score(self, x_test, t_target):
        """
        Generic score and log loss function
        :param x_test: test data
        :param t_target: prediction test data
        :return: the score and the log loss of the classifier on the test data
        """
        t_predicted = self.classifier.predict_proba(x_test)
        # print(x_test.shape, t_target.shape)
        # print(t_predicted.shape)
        return self.classifier.score(x_test, t_target), log_loss(t_target, t_predicted)
