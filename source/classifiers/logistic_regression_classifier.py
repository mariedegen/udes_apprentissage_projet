# -*- coding: utf-8 -*-
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV

from classifiers.generic_classifier import GenericClassifier

"""
    Logistic Regression Classifier
"""


class LogisticRegressionClassifier(GenericClassifier):
    def __init__(self):
        """
        Constructor
        self.classifier : type of classifier
        :return:
        """
        super().__init__()

    def training(self, x_train, t_train):
        """
        Logistic Regression classifier with scikit learn
        :param x_train: training set
        :param t_train: prediction
        """
        # init the LogisticRegression classifier
        logistic_regression_classifier = LogisticRegression(solver='lbfgs', multi_class="auto", max_iter=500)

        # create a dictionary with different values for Hyper Parameters
        param_grid = {"C": np.arange(1, 10, 2)}

        # Grid Search with K-fold Cross validation (cv)
        grid = GridSearchCV(logistic_regression_classifier, param_grid, cv=7, scoring='accuracy', n_jobs=-1, iid=False)
        grid.fit(x_train, t_train)

        self.classifier = grid

        # print the best parameters found with the GS
        print("Best parameters for the Random Forest classifier : ", grid.best_params_)
