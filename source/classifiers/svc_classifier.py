# -*- coding: utf-8 -*-

from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC

from classifiers.generic_classifier import GenericClassifier

"""
    SVM Classifier with kernels
"""


class SVCClassifier(GenericClassifier):

    def __init__(self):
        """
        Constructor
        self.classifier : type of classifier
        :return:

        """
        super().__init__()

    def training(self, x_train, t_train):
        """
        SVC with a rbf kernel, using scikit learn
        :param x_train: training set
        :param t_train: prediction

        """
        # init the SVC classifier with rbf kernel
        svc = SVC(kernel='rbf', probability=True)

        # create a dictionary with different values for the regularization and gamma
        param_grid = {'C': [0.001, 0.01, 0.1, 1, 10], 'gamma': [0.001, 0.01, 0.1, 1]}

        # Grid Search with K-fold Cross validation (cv)
        svc_clf = GridSearchCV(estimator=svc, param_grid=param_grid, cv=7, n_jobs=-1, iid=False)

        svc_clf.fit(x_train, t_train)
        self.classifier = svc_clf

        # print the best parameters found with the GS
        print('Best parameters for the SVC classifier with RBF kernel : ', svc_clf.best_params_)
