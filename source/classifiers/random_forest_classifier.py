# -*- coding: utf-8 -*-

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV

from classifiers.generic_classifier import GenericClassifier

"""

    Random Forest Classifier

"""


class RandomForest(GenericClassifier):

    def __init__(self):
        """
        Constructor
        self.classifier : type of classifier
        :return:

        """
        super().__init__()

    def training(self, x_train, t_train):
        """
        Random Forest classifier with scikit learn
        :param x_train: training set
        :param t_train: prediction

        """
        # init the RF classifier
        rf_clf = RandomForestClassifier(n_jobs=-1, bootstrap=True)

        # create a dictionary with different values for n_estimators 
        param_grid = {'n_estimators': [50, 100, 200, 300]}

        # Grid Search with K-fold Cross validation (cv)
        grid = GridSearchCV(rf_clf, param_grid, cv=7, scoring='accuracy', n_jobs=-1, iid=False)
        grid.fit(x_train, t_train)

        self.classifier = grid

        # print the best parameters found with the GS
        print("Best parameters for the Random Forest classifier : ", grid.best_params_)
