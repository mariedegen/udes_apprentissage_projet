# -*- coding: utf-8 -*-

from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier

from classifiers.generic_classifier import GenericClassifier

"""

    K-Nearest Neighbors Classifier
    
"""


class KNNClassifier(GenericClassifier):

    def __init__(self):
        """
        Constructor
        self.classifier : type of classifier
        self.k : number of nearest neighbors define at 3 by default
        :return:

        """
        super().__init__()
        self.k = 3

    def training(self, x_train, t_train):
        """
        KNN classifier with scikit learn
        :param x_train: training set
        :param t_train: prediction

        """
        # init the k-NN classifier
        knn = KNeighborsClassifier()

        # create a dictionary with different values for the NN and for the distance metric
        param_grid = {
            'n_neighbors': [1, 3, 5, 7, 9, 11, 20],
            'metric': ['euclidean', 'manhattan', 'minkowski']
        }

        # Grid Search with K-fold Cross validation (cv)
        grid = GridSearchCV(knn, param_grid, cv=7, scoring='accuracy', n_jobs=-1, iid=False)
        grid.fit(x_train, t_train)

        self.k = grid.best_params_
        self.classifier = grid

        # print the best parameters found with the GS
        print("Best parameters for the K-NN classifier : ", self.k)
