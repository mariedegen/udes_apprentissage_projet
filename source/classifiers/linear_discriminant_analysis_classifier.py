# -*- coding: utf-8 -*-
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

from classifiers.generic_classifier import GenericClassifier

"""
    Linear Discriminant Analysis Classifier
"""


class LinearDiscriminantAnalysisClassifier(GenericClassifier):
    def __init__(self):
        """
        Constructor
        self.classifier : type of classifier
        :return:
        """
        super().__init__()

    def training(self, x_train, t_train):
        """
        Linear Discriminant Analysis classifier with scikit learn
        :param x_train: training set
        :param t_train: prediction
        """
        # init the Linear Discriminant Analysis classifier and train
        # No GridSearch needed, there is no hyper parameter
        linear_discriminant_classifier = LinearDiscriminantAnalysis()
        linear_discriminant_classifier.fit(x_train, t_train)

        self.classifier = linear_discriminant_classifier
