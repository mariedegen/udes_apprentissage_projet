# -*- coding: utf-8 -*-

from argparse import ArgumentParser

import pandas as pd

from classifiers.gaussian_nb_classifier import GaussianNaiveBayesClassifier
from classifiers.kNN_classifier import KNNClassifier
from classifiers.linear_discriminant_analysis_classifier import LinearDiscriminantAnalysisClassifier
from classifiers.logistic_regression_classifier import LogisticRegressionClassifier
from classifiers.random_forest_classifier import RandomForest
from classifiers.svc_classifier import SVCClassifier
from model.file_reader import FileReader

if __name__ == "__main__":
    # Args from the user
    parser = ArgumentParser()
    parser.add_argument("train_file", help="training csv file")
    parser.add_argument("test_file", help="testing csv file")
    parser.add_argument('--skip-comparison', dest="skip_comparison", help='Disable classifiers comparison', action="store_true")

    args = parser.parse_args()
    train_file = args.train_file
    test_file = args.test_file
    skip_comparison = args.skip_comparison

    # Read files
    data = FileReader.read_files(train_file, test_file)

    classifiers = [
        KNNClassifier(),
        SVCClassifier(),
        RandomForest(),
        LogisticRegressionClassifier(),
        GaussianNaiveBayesClassifier(),
        LinearDiscriminantAnalysisClassifier()
    ]

    x_full, y_full = data.x_train, data.y_train
    x_train, x_test, y_train, y_test = data.shuffle_split()
    for classifier in classifiers:
        name = classifier.__class__.__name__

        print("-----------------------------------")
        print(" Running " + name + ".")
        print("-----------------------------------")

        if not skip_comparison:
            # best_prediction = -9999
            print("Comparison output :")
            classifier.training(x_train, y_train)

            # compute train score and test score for splitted training dataset
            train_score, log_loss = classifier.score(x_train, y_train)
            print('Train Score :', train_score * 100, '%')
            print('Train Log loss:', log_loss)
            print("")

            test_score, log_loss = classifier.score(x_test, y_test)
            print('Test Score :', test_score * 100, '%')
            print('Test Log loss:', log_loss)
            print("")

        # Now we train on all the dataset for kaggle prediction
        print("Kaggle training :")
        classifier.training(x_train, y_train)
        prediction = classifier.prediction(data.x_test)
        submission = pd.DataFrame(prediction, index=data.test_ids, columns=data.classes)
        filename = 'submissions/submission_' + name + '.csv'
        submission.to_csv(filename)
        print("Scores saved in", filename, "\n")
